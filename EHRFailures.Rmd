---
title: CCMG and CCP EHR Failures in the Last Two Months
output:
  flexdashboard::flex_dashboard:
    orientation: rows
    css: custom.css
    logo: acuitas.png
---
```{r,echo=F,warning=F,message=F}
library(DBI)
library(odbc)
library(dplyr)
library(ggplot2)
library(kableExtra)
library(knitr)
library(DT)
library(plotly)
library(crosstalk)
library(tidyverse)
library(yaml)

```


```{r,echo=F}


secrets = "~/secrets.yaml"
#srv = "acny-edwprod"
yml <- read_yaml(secrets)
connect_db <- function (yml, srv) {
   con <- odbc::dbConnect(
     odbc::odbc(),
     Driver = yml[[srv]]$odbc_driver,
     Server = yml[[srv]]$server,
     uid = yml[[srv]]$domain_user,
     pwd = yml[[srv]]$password,
     Port = yml[[srv]]$port
   )
  return(con)
}
```


```{r,echo=F}
con_prod = connect_db(yml,"acny-edwprod")
#conn <- lapply(c("acny-edwprod","acny-edwdev"),connect_db)

## In secrets.yaml (line 7 in my file):
## domain_user: &duser "ACEDW\\Andy.Choens"
## and then references to:
## domain_user: *duser
## in the other databases.

```
```{r,echo=F}
connList = list("acny-edwdev","acny-edwprod")
frames = c()
query = "
  DECLARE @lookback int
SET @lookback = 2
select db.DatamartNM [Database]
--	,bh.*
	,bh.BatchDefinitionNM [Batch Name]
	,CONVERT(varchar(19),StartDTS ,120) [Start time]
	,CONVERT(varchar(19),EndDTS,120) [End time]
	,CAST(bh.DurationSecondsCNT as float) / 60 / 60 [Runtime (hrs)] 
	,BatchSourceTXT [Run Type]
	,CreationDTS [Batch Creation Date]
	,StatusCD [Batch Status]
from EDWAdmin.CatalystAdmin.ETLBatchHistoryBASE bh
inner join EDWAdmin.CatalystAdmin.DataMartBASE db
	on db.DatamartID = bh.DataMartID
where 1=1
	AND db.DataMartNM like '%EHR'
	AND bh.CreationDTS > DATEADD(month, -@lookback, GETDATE())
ORDER BY db.DataMartNM, bh.StartDTS ASC
"

for (conname in connList) {
  con = connect_db(yml,conname)
  results = dbGetQuery(con, query)
  results["Runtime (hrs)"] <-round(results["Runtime (hrs)"],2)
  results = list(results)
  names(results) = conname
  frames = c(frames,results)
  dbDisconnect(con)
  
}

```




```{r}
pop1 = frames["acny-edwprod"][[1]]
pop2 = frames["acny-edwdev"][[1]]
pop1summary = pop1%>%group_by(Database,`Batch Status`)%>%summarize(n=n())
CCPfailsPROD = pop1summary%>%filter(`Database`=='CCPAllScriptsEHR',`Batch Status`=='Failed')%>%select(n)
CCMGfailsPROD = pop1summary%>%filter(`Database`=='CapitalCareAllScriptsEHR',`Batch Status`=='Failed')%>%select(n)
pop2summary = pop2%>%group_by(Database,`Batch Status`)%>%summarize(n=n())
CCPfailsDEV = pop2summary%>%filter(Database=='CCPAllScriptsEHR',`Batch Status`=='Failed')%>%select(n)
CCMGfailsDEV = pop2summary%>%filter(Database=='CapitalCareAllScriptsEHR',`Batch Status`=='Failed')%>%select(n)
PRODfailPCT = (CCPfailsPROD$n + CCMGfailsPROD$n)/nrow(pop2)
DEVfailPCT = (CCPfailsDEV$n + CCMGfailsDEV$n)/nrow(pop2)
pop1 = pop1%>%filter(`Batch Status`=="Failed")
pop2 = pop1%>%filter(`Batch Status`=="Failed")
pop1$Server = "[PROD]"
pop2$Server = "[DEV]"

fail_df = rbind(pop1,pop2)



```




Row 
----------------------------------------------------

```{r setup, include=FALSE}
library(flexdashboard)

```


### PROD CCMG # of Failures

```{r}
valueBox(CCMGfailsPROD$n, icon = "fa-filter", color = "#5e3294")
#filter pop1 to CCMG failures
```

### PROD CCP # of Failures

```{r}
valueBox(CCPfailsPROD$n, icon = "fa-filter", color = "#2E9E3F")
#filter pop1 to CCMG failures
```

### PROD CCMG # of Failures

```{r}
valueBox(CCMGfailsDEV$n, icon = "fa-filter", color = "#5e3294")
#filter pop1 to CCMG failures
```

### PROD CCP # of Failures

```{r}
valueBox(CCPfailsDEV$n, icon = "fa-filter", color = "#2E9E3F")
#filter pop1 to CCMG failures
```

### % DEV failures

```{r}
gauge(as.integer(DEVfailPCT*100), min = 0, max = 100, symbol = '%', gaugeSectors(
  success = c(80, 100), warning = c(40, 79), danger = c(0, 39)
))
```

### % PROD Failures

```{r}
gauge(as.integer(PRODfailPCT*100), min = 0, max = 100, symbol = '%', gaugeSectors(
  success = c(80, 100), warning = c(40, 79), danger = c(0, 39)
))
```

```{r}
create_dt <- function(x){
datatable(x,
                extensions = 'Buttons',
                options = list(dom = 'Blfrtip',
                               pageLength =  100,
                               filter = list(position = 'top', clear = FALSE),
                               buttons = c('copy', 'csv', 'excel', 'pdf', 'print'),
                               lengthMenu = list(c(10,25,50,-1),
                                                 c(10,25,50,"All")))) %>%
                formatRound(columns=c('Runtime (hrs)'), digits=3)
}

```

Row {.tabset .tabset-fade} 
----------------------------------------------------
### rPivot




```{r}
library(rpivotTable)
rpivotTable(fail_df,rows=c("Database", "Batch Name"), cols=c("Batch Status","Server"),width="100%", height="400px")

```


### CCP/CCMG EHR Failures [PROD]

```{r,echo=F, results='asis'}
create_dt(pop1)

```

### CCP/CCMG EHR Failures [DEV]

```{r,echo=F, results='asis'}
create_dt(pop2)

```

<!-- ### Barplot -->

<!-- ```{r} -->
<!-- p <- byDx%>% -->
<!--    ggplot(aes(x=SubCareProcessNM, y=membrs)) + -->
<!--    geom_bar(stat="identity") +  -->
<!--    coord_flip() -->

<!-- ggplotly(p) -->

<!-- ``` -->



<!-- ### Crosstalk barplot -->
<!-- Drag and drop column names to arrang table rows and columns  -->
<!-- ```{r} -->
<!-- shared_byDx <- SharedData$new(byDx) -->
<!-- p <- shared_byDx%>% -->
<!--    ggplot(aes(x=SubCareProcessNM, y=membrs)) + -->
<!--    geom_bar(stat="identity") +  -->
<!--    coord_flip() -->

<!-- # bscols(widths = c(2,10), -->
<!-- #   list( -->
<!--     filter_slider("membrs", "membrs", shared_byDx, ~membrs, width = "100%") -->
<!--     filter_select("SubCareProcessNM", "SubCareProcessNM", shared_byDx, ~SubCareProcessNM) -->
<!--   # ), -->
<!-- ggplotly(p) -->
<!-- # ) -->

<!-- ``` -->











